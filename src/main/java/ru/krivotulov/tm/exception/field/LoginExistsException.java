package ru.krivotulov.tm.exception.field;

/**
 * LoginEmptyException
 *
 * @author Aleksey_Krivotulov
 */
public final class LoginExistsException extends AbstractFieldException {

    public LoginExistsException() {
        super("Error! user with this login not found...");
    }

}
