package ru.krivotulov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * TaskUpdateByIndexCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-update-by-index";

    @NotNull
    public static final String DESCRIPTION = "Update task by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.readLine();
        @NotNull final String userId = getUserId();
        getTaskService().updateByIndex(userId, index, name, description);
    }

}
