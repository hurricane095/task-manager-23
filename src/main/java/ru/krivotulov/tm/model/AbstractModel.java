package ru.krivotulov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 * AbstractEntity
 *
 * @author Aleksey_Krivotulov
 */

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractModel {

    @NotNull
    private String id = UUID.randomUUID().toString();

}
