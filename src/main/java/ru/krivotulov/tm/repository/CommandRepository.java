package ru.krivotulov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.ICommandRepository;
import ru.krivotulov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public void add(@NotNull AbstractCommand command) {
        @NotNull final String name = command.getName();
        mapByName.put(name, command);
        @NotNull final String argument = command.getArgument();
        mapByArgument.put(argument, command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return mapByName.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        return mapByArgument.get(argument);
    }

}
