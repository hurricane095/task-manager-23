package ru.krivotulov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IUserRepository;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;
import ru.krivotulov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * UserRepository
 *
 * @author Aleksey_Krivotulov
 */
public class UserRepository extends AbstractRepository<User> implements IUserRepository {


    @NotNull
    @Override
    public User create(@NotNull final String login,
                       @NotNull final String password
    ) {
        @NotNull final User user = new User();
        user.setRole(Role.USUAL);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return add(user);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login,
                       @NotNull final String password,
                       @NotNull final String email
    ) {
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login,
                       @NotNull final String password,
                       @NotNull final Role role
    ) {
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return models
                .stream()
                .filter(model -> login.equals(model.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return models
                .stream()
                .filter(model -> email.equals(model.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
