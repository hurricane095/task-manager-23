package ru.krivotulov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IRepository;
import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.model.AbstractModel;

import java.util.List;

/**
 * IService
 *
 * @author Aleksey_Krivotulov
 */
public interface IService<M extends AbstractModel> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable final Sort sort);
    
}
