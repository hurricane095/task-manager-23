package ru.krivotulov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * ICommand
 *
 * @author Aleksey_Krivotulov
 */
public interface ICommand {

    @NotNull
    String getName();

    @NotNull
    String getDescription();

    @Nullable
    String getArgument();

}
