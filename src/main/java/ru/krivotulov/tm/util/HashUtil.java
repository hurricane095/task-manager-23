package ru.krivotulov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * HashUtil
 *
 * @author Aleksey_Krivotulov
 */
public interface HashUtil {

    @NotNull
    String SECRET = "1234";

    @NotNull
    Integer ITERATION = 9876;

    @Nullable
    static String salt(@Nullable final String password) {
        if (password == null) return null;
        @Nullable String result = password;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    @Nullable
    static String md5(@Nullable final String val) {
        if (val == null) return null;
        try {
            @NotNull MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(val.getBytes());
            @NotNull final StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                stringBuffer.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return stringBuffer.toString();
        } catch (@NotNull final NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
